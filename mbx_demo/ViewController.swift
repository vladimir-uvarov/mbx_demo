//
//  ViewController.swift
//  mbx_demo
//
//  Created by Владимир Уваров on 23.06.15.
//  Copyright (c) 2015 Владимир Уваров. All rights reserved.
//

import UIKit
import MapboxGL
import CoreLocation

class ViewController: UIViewController, MGLMapViewDelegate, CLLocationManagerDelegate {
    @IBOutlet weak var navButt: UIButton!
    
    var locationManager:CLLocationManager!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let mapView = MGLMapView(frame: view.bounds)
        
        // Do any additional setup after loading the view, typically from a nib.
        if (locationManager != nil) {
        
            let location = locationManager.location
            let coord = location.coordinate
            let latitude = coord.latitude
            let longitude = coord.latitude
        }

        MGLAccountManager.setAccessToken("pk.eyJ1IjoianVzdGluIiwiYSI6IlpDbUJLSUEifQ.4mG8vhelFMju6HpIY-Hi5A")
        
        
        
        mapView.userTrackingMode = .Follow
       
        // set the map's center coordinate
        
       
        view.addSubview(mapView)
      
        view.bringSubviewToFront(navButt)

    }
    
    @IBAction func navButt(sender: AnyObject) {
        // let loc = locationManager.location
        self
        locationManager = CLLocationManager()
        locationManager.delegate = self
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        locationManager.requestWhenInUseAuthorization()
        locationManager.startUpdatingLocation()
        
       
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func locationManager(manager: CLLocationManager!, didUpdateLocations locations: [AnyObject]!)
    {
        
        CLGeocoder().reverseGeocodeLocation(manager.location, completionHandler: {(placemarks, error)->Void in
            
            if (error != nil)
            {
                println("Error: " + error.localizedDescription)
                return
            }
            
            if placemarks.count > 0
            {
                let pm = placemarks[0] as! CLPlacemark
                self.displayLocationInfo(pm)
            }
            else
            {
                println("Error with the data.")
            }
        })
    }
    
    func displayLocationInfo(placemark: CLPlacemark)
    {
       
        self.locationManager.stopUpdatingLocation()
        let latitude = placemark.location.coordinate.latitude
        let longitude = placemark.location.coordinate.longitude
        
        
        let alert = UIAlertController(title: "Координаты", message: "Широта:" + String(stringInterpolationSegment :latitude) + "\n\nДолгота:" + String(stringInterpolationSegment :longitude),preferredStyle: UIAlertControllerStyle.Alert)
        alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.Default,
            handler: nil))
        
        self.presentViewController(alert, animated: true, completion: nil)
        
    }
    
    func locationManager(manager: CLLocationManager!, didFailWithError error: NSError!)
    {
        println("Error: " + error.localizedDescription)
    }
    
    

    
}
