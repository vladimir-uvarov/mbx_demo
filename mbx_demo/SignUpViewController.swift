//
//  SignUpViewController.swift
//  mbx_demo
//
//  Created by Владимир Уваров on 25.06.15.
//  Copyright (c) 2015 Владимир Уваров. All rights reserved.
//

import UIKit
import Parse

class SignUpViewController: UIViewController {

    @IBOutlet weak var SignUpButton: UIButton!
    @IBOutlet weak var loginField: UITextField!
    @IBOutlet weak var passwdField: UITextField!
    @IBOutlet weak var cpasswdField: UITextField!
    @IBOutlet weak var groupField: UITextField!
    @IBOutlet weak var leaderInd: UISwitch!
    override func viewDidLoad() {
        
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    
    @IBAction func SignUpButton(sender: UIButton) {
        if cpasswdField.text == passwdField.text && passwdField.text != nil && groupField.text != nil{
            
            var user = PFUser()
            user.username = loginField.text
            user.password = passwdField.text
            user["group"] = groupField.text
            user["leader"] = leaderInd.on
            
            user.signUpInBackgroundWithBlock {
                (succeeded: Bool, error: NSError?) -> Void in
                if (error != nil) {
                  
                    let alert = UIAlertController(title: "Ошибка!", message: error?.localizedDescription ,preferredStyle: UIAlertControllerStyle.Alert)
                    alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.Default,
                        handler: nil))
                    self.presentViewController(alert, animated: true, completion: nil)
                    
                } else {
                    
                    let alert = UIAlertController(title: "Регистрация прошла успешно!", message: "" ,preferredStyle: UIAlertControllerStyle.Alert)
                    alert.addAction(UIAlertAction(title: "Войти", style: UIAlertActionStyle.Default,
                        handler: { (action: UIAlertAction!) -> () in
                            self.performSegueWithIdentifier("RegToLogin", sender: self)
                        }))
                    self.presentViewController(alert, animated: true, completion: nil)
                    
                }
            }
        } else {
            
           
        }
       

    }
    
    
    
   
  
   }
