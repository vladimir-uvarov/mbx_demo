//
//  SignInViewController.swift
//  mbx_demo
//
//  Created by Владимир Уваров on 25.06.15.
//  Copyright (c) 2015 Владимир Уваров. All rights reserved.



import UIKit
import Parse
class SignInViewController: UIViewController {
    
    @IBOutlet weak var loginField: UITextField!
    @IBOutlet weak var passwdField: UITextField!
    @IBOutlet weak var SignInButton: UIButton!
    @IBOutlet weak var groupField: UITextField!
    @IBOutlet weak var groupInd: UIButton!
    @IBOutlet weak var navItem: UINavigationItem!
    override func viewDidLoad() {
        
        super.viewDidLoad()

    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    @IBAction func SignInAct(sender: UIButton) {
        
    
   

        PFUser.logInWithUsernameInBackground(loginField.text, password:passwdField.text) {
            (user: PFUser?, error: NSError?) -> Void in
            
            if user != nil {
                
               self.performSegueWithIdentifier("SignInToMaps", sender: self)
            //    var f = user?["leader"] as! String
                
              //  self.navItem.title = f

                
            } else {
                
                let alert = UIAlertController(title: "Ошибка!", message: error?.localizedDescription ,preferredStyle: UIAlertControllerStyle.Alert)
                alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.Default,
                    handler: nil))
                self.presentViewController(alert, animated: true, completion: nil)
            }
        }
        
        }
 
   
    
}
